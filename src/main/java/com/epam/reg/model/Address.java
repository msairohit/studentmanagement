/**
 * 
 */
package com.epam.reg.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

/**
 * @author Daljit_Singh
 *
 */
@Component
@XmlRootElement
public class Address {

    private String id;
    private String type;
    private String houseNumber;
    private String street;
    private String landmark;
    private String city;
    private String state;
    private String zipcode;
    
    
    /**
	 * 
	 */
	public Address() {
		super();
	}
	/**
     * @param id
     * @param type
     * @param houseNumber
     * @param street
     * @param landmark
     * @param city
     * @param state
     * @param zipcode
     */
    public Address(String id, String type, String houseNumber, String street, String landmark, String city,
            String state, String zipcode) {
        super();
        this.id = id;
        this.type = type;
        this.houseNumber = houseNumber;
        this.street = street;
        this.landmark = landmark;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the houseNumber
     */
    public String getHouseNumber() {
        return houseNumber;
    }
    /**
     * @param houseNumber the houseNumber to set
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }
    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }
    /**
     * @return the landmark
     */
    public String getLandmark() {
        return landmark;
    }
    /**
     * @param landmark the landmark to set
     */
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @return the state
     */
    public String getState() {
        return state;
    }
    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }
    /**
     * @return the zipcode
     */
    public String getZipcode() {
        return zipcode;
    }
    /**
     * @param zipcode the zipcode to set
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    
    
}
