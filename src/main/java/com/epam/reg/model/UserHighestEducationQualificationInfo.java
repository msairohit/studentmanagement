/**
 * 
 */
package com.epam.reg.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

/**
 * @author Daljit_Singh
 *
 */
@Component
@XmlRootElement
public class UserHighestEducationQualificationInfo {
    private String id;
    private String collegeName;
    private String university;
    private String passOutYear;
    private String branch;
    private float percentageOfMarks;
    
    
    /**
	 * 
	 */
	public UserHighestEducationQualificationInfo() {
		super();
	}
	/**
     * @param id
     * @param collegeName
     * @param university
     * @param passOutYear
     * @param branch
     * @param percentageOfMarks
     */
    public UserHighestEducationQualificationInfo(String id, String collegeName, String university, String passOutYear,
            String branch, float percentageOfMarks) {
        super();
        this.id = id;
        this.collegeName = collegeName;
        this.university = university;
        this.passOutYear = passOutYear;
        this.branch = branch;
        this.percentageOfMarks = percentageOfMarks;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the collegeName
     */
    public String getCollegeName() {
        return collegeName;
    }
    /**
     * @param collegeName the collegeName to set
     */
    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }
    /**
     * @return the university
     */
    public String getUniversity() {
        return university;
    }
    /**
     * @param university the university to set
     */
    public void setUniversity(String university) {
        this.university = university;
    }
    /**
     * @return the passOutYear
     */
    public String getPassOutYear() {
        return passOutYear;
    }
    /**
     * @param passOutYear the passOutYear to set
     */
    public void setPassOutYear(String passOutYear) {
        this.passOutYear = passOutYear;
    }
    /**
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }
    /**
     * @param branch the branch to set
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }
    /**
     * @return the percentageOfMarks
     */
    public float getPercentageOfMarks() {
        return percentageOfMarks;
    }
    /**
     * @param percentageOfMarks the percentageOfMarks to set
     */
    public void setPercentageOfMarks(float percentageOfMarks) {
        this.percentageOfMarks = percentageOfMarks;
    }
    
    
}
