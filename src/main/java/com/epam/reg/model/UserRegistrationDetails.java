package com.epam.reg.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement
public class UserRegistrationDetails {
    
    private String id;
    private String emailId;
    private String password;
    private String status;
    private String createdDate;
    private String updatedDate;
    
    
    /**
	 * 
	 */
	public UserRegistrationDetails() {
		super();
	}

	/**
     * @param emailId
     * @param password
     * @param status
     * @param createdDate
     * @param updatedDate
     */
    public UserRegistrationDetails(String id, String emailId, String password, String status, String createdDate,
            String updatedDate) {
        super();
        this.id = id;
        this.emailId = emailId;
        this.password = password;
        this.status = status;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }
    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }
    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    /**
     * @return the updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }
    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserRegistrationDetails [id=" + id + ", emailId=" + emailId + ", password=" + password + ", status="
				+ status + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + "]";
	}
    
}
