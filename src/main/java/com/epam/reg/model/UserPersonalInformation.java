/**
 * 
 */
package com.epam.reg.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

/**
 * @author Daljit_Singh
 *
 */
@Component
@XmlRootElement
public class UserPersonalInformation {
    private String id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String mobileNumber;
    private String alternateNumber;
    
    /**
	 * 
	 */
	public UserPersonalInformation() {
		super();
	}
	/**
     * @param id
     * @param firstName
     * @param middleName
     * @param lastName
     * @param dateOfBirth
     * @param mobileNumber
     * @param alternateNumber
     */
    public UserPersonalInformation(String id, String firstName, String middleName, String lastName, String dateOfBirth,
            String mobileNumber, String alternateNumber) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.mobileNumber = mobileNumber;
        this.alternateNumber = alternateNumber;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }
    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    /**
     * @return the alternateNumber
     */
    public String getAlternateNumber() {
        return alternateNumber;
    }
    /**
     * @param alternateNumber the alternateNumber to set
     */
    public void setAlternateNumber(String alternateNumber) {
        this.alternateNumber = alternateNumber;
    }
    
    
}
