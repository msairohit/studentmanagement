/**
 * 
 */
package com.epam.reg.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

/**
 * @author Daljit_Singh
 *
 */
@Component
@XmlRootElement
public class AcademicDetails {
    
    private String id;
    private String name;
    private String board;
    private String passOutYear;
    private float percentage;
    
    
    /**
	 * 
	 */
	public AcademicDetails() {
		super();
	}
	/**
     * @param id
     * @param name
     * @param board
     * @param passOutYear
     * @param percentage
     */
    public AcademicDetails(String id, String name, String board, String passOutYear, float percentage) {
        super();
        this.id = id;
        this.name = name;
        this.board = board;
        this.passOutYear = passOutYear;
        this.percentage = percentage;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the board
     */
    public String getBoard() {
        return board;
    }
    /**
     * @param board the board to set
     */
    public void setBoard(String board) {
        this.board = board;
    }
    /**
     * @return the passOutYear
     */
    public String getPassOutYear() {
        return passOutYear;
    }
    /**
     * @param passOutYear the passOutYear to set
     */
    public void setPassOutYear(String passOutYear) {
        this.passOutYear = passOutYear;
    }
    /**
     * @return the percentage
     */
    public float getPercentage() {
        return percentage;
    }
    /**
     * @param percentage the percentage to set
     */
    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }
    
    
}
