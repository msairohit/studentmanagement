package com.epam.reg.rest.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.reg.model.UserPersonalInformation;
import com.epam.reg.rest.dao.UserPersonalInfoDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserPersonalInfoService {
    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    UserPersonalInfoDAO userPersonalInfoDAO;
    
    public boolean insertUserPersonalInformation(String details) throws JsonParseException, JsonMappingException, IOException {
        UserPersonalInformation info = mapper.readValue(details, UserPersonalInformation.class);
        return userPersonalInfoDAO.insertData(info);
    }
}
