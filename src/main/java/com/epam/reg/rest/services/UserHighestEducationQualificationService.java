package com.epam.reg.rest.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.reg.model.UserHighestEducationQualificationInfo;
import com.epam.reg.rest.dao.UserHighestEducationDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserHighestEducationQualificationService {
    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    UserHighestEducationDAO userHighestEducationDAO;
    
    public boolean insertEducationDetails(String details) throws JsonParseException, JsonMappingException, IOException {
        UserHighestEducationQualificationInfo info = mapper.readValue(details, UserHighestEducationQualificationInfo.class);
        return userHighestEducationDAO.insertData(info);
    }
}
