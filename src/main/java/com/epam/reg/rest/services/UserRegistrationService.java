/**
 * 
 */
package com.epam.reg.rest.services;

import java.io.IOException;
import java.time.LocalDate;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.reg.model.UserRegistrationDetails;
import com.epam.reg.rest.dao.UserRegistrationDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Sai_Manikonda
 *
 */
@Service
public class UserRegistrationService {

	@Autowired
	UserRegistrationDetails userRegistrationDetails;
	
	@Autowired
	UserRegistrationDAO userRegistrationDAO;
	
	@Autowired
	ObjectMapper mapper;
	public void insertDetails(String details) throws JsonParseException, JsonMappingException, IOException, ParseException, JSONException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(details);
		String todaysDate = java.time.LocalDate.now().toString();
		
		json.put("status", "ACTIVE");
		json.put("createdDate", todaysDate);
		json.put("updatedDate", todaysDate);
		int size = (userRegistrationDAO.findTotalUsers());
		System.out.println("returned size :" + size);
		++size;
		System.out.println("after incrementing :" + size);
		json.put("id", size);
		
		userRegistrationDetails = mapper.readValue(json.toString(), UserRegistrationDetails.class);
		userRegistrationDAO.insertStudentRegistrationDetails(userRegistrationDetails);
		
	}

}
