package com.epam.reg.rest.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.reg.model.Address;
import com.epam.reg.rest.dao.AddressDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AddressService {
    
    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    AddressDAO addressDAO;
    
    public boolean insertAddress(String details) throws JsonParseException, JsonMappingException, IOException {
        Address address = mapper.readValue(details, Address.class);
        return addressDAO.insertData(address);
    }
}
