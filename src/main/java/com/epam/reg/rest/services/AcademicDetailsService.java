package com.epam.reg.rest.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.reg.model.AcademicDetails;
import com.epam.reg.rest.dao.AcademicDetailsDAO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AcademicDetailsService {
    
    @Autowired
    ObjectMapper mapper;
    
    @Autowired
    AcademicDetailsDAO academicDetailsDAO;
    
    public boolean insertAcademicDetails(String details) throws JsonParseException, JsonMappingException, IOException {
        AcademicDetails academicDetails = mapper.readValue(details, AcademicDetails.class); 
        return academicDetailsDAO.insertData(academicDetails);
    }
}
