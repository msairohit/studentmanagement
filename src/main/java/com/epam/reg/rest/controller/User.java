/**
 * 
 */
package com.epam.reg.rest.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.reg.rest.services.AcademicDetailsService;
import com.epam.reg.rest.services.AddressService;
import com.epam.reg.rest.services.UserHighestEducationQualificationService;
import com.epam.reg.rest.services.UserPersonalInfoService;
import com.epam.reg.rest.services.UserRegistrationService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;



/**
 * @author Sai_Manikonda
 *
 */
@Produces(MediaType.APPLICATION_JSON)
@RestController
public class User {
	
	@Autowired
	UserRegistrationService userRegistrationService;
	
	@Autowired
	AcademicDetailsService academicDetailsService;
	
	@Autowired
	AddressService addressService;
	
	@Autowired
	UserHighestEducationQualificationService userHighestEducationService;
	
	@Autowired
	UserPersonalInfoService userPersonalInfoService;
	
	

	@PostMapping("/user")
	public String insertUserRegistrationDetails(@RequestBody String details) throws JsonParseException, JsonMappingException, IOException, ParseException, JSONException {
		userRegistrationService.insertDetails(details);
		return "hello";
	}
	
	@PostMapping("/user/{id}/personalInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertUserPersonalDetails(@PathVariable("id") String id, @RequestBody String details) throws ParseException, JsonParseException, JsonMappingException, IOException, URISyntaxException {
		System.out.println(id);
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(details);
		JSONObject academicDetails = (JSONObject) json.get("academicDetails");
		academicDetails.put("id", id);
		JSONObject address = (JSONObject) json.get("address");
		address.put("id", id);
		JSONObject userHighestEducation = (JSONObject) json.get("userHighestEducation");
		userHighestEducation.put("id", id);
		JSONObject userPersonalInformation = (JSONObject) json.get("userPersonalInformation");
		userPersonalInformation.put("id", id);
		
	    academicDetailsService.insertAcademicDetails(academicDetails.toString());
	    addressService.insertAddress(address.toString());
	    userHighestEducationService.insertEducationDetails(userHighestEducation.toString());
	    userPersonalInfoService.insertUserPersonalInformation(userPersonalInformation.toString());
		
	    
	    JSONObject message = new JSONObject();
	    message.put("emailId", "daljittt");
	    
		Response.ResponseBuilder builder = Response.status(Response.Status.CREATED);
		builder.entity(message);
	    return builder.build();
	}
	
}
