/**
 * 
 */
package com.epam.reg.rest.dao;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.epam.reg.configuration.MongoConfiguration;
import com.epam.reg.model.UserRegistrationDetails;
import com.mongodb.client.MongoCollection;

/**
 * @author Sai_Manikonda
 *
 */
@Repository
public class UserRegistrationDAO {
	
	@Autowired
	UserRegistrationDetails userRegistrationDetails;
	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfiguration.class);
    MongoOperations mongoOperation = (MongoOperations)ctx.getBean("mongoTemplate");
    
    public int findTotalUsers() {
    	MongoCollection<Document> collection = mongoOperation.getCollection("USER_REGISTRATION");
    	long all = collection.count();
    	int value = (int) all;
    	System.out.println("size is :" + value);
    	return value;
    }
    
    public void insertStudentRegistrationDetails(UserRegistrationDetails object) {
    	mongoOperation.insert(object, "USER_REGISTRATION");
    }
    
}
