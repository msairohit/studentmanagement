package com.epam.reg.rest.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import com.epam.reg.configuration.MongoConfiguration;
import com.epam.reg.model.UserPersonalInformation;
import com.mongodb.MongoServerException;

@Repository
public class UserPersonalInfoDAO {
    public boolean insertData(UserPersonalInformation details) {
        boolean isInserted = true;
        ApplicationContext ctx = 
                new AnnotationConfigApplicationContext(MongoConfiguration.class);
       MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
       try {
           mongoOperation.save(details, "USER_PERSONAL_INFO");
       } catch (MongoServerException ex) {
           isInserted = false;
       }
       return isInserted;
    }
}
